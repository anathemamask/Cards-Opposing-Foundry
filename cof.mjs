/**
 * Process all necessary imports.
 * THIS IS WHERE I'D PUT MY import { SOMETHING } from "./modules/SOMETHING.mjs";
 * IF I HAD ANY
 */
import { CardsLayer } from "./modules/cardslayer.mjs"
/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.on("init", () => {
    if ((!canvas.scene.getFlag("CardsLayer", "CanvasCards")) && canvas.scene.active) {
    canvas.scene.setFlag("CardsLayer", "CanvasCards")
    }
    CONFIG.Canvas.layers["CardsLayer"] = {
        layerClass: CardsLayer,
        group: "interface"
    };
});

Hooks.once("init", function() {
    //Init hooks? Okay well if we're doing this
    CONFIG.debug.hooks = true
    console.log("Loaded Cards Opposing Foundry Game System!")
})

/**
 * Hook canvas drop events to determine if the dropped object
 * was a card. If so, do some card things with it.
 *
 */
Hooks.on("dropCanvasData", function(event, data) {
    //Catch when something is dropped onto the canvas
    // and determine if it's a card
    //console.log(data)
    if (data.type === "Card") {

        return _onDropCardData(event, data)
    }
})
/**
 * Handle dropping of Card data onto the Scene canvas
 * @private
 */
async function _onDropCardData(event, data) {
    // Ensure the user has permission to drop the actor and create a Token
    if ( !game.user.can("TOKEN_CREATE") ) {
        return ui.notifications.warn(`You do not have permission to create new Tokens!`);
    }
    /*
    let parent = game.cards.get(data.cardsId)
    let card = parent.data.cards.get(data.cardId)
    let cData = {card.data}
    let cTokenData = {
        img: cData.img,
        height: cData.height,
        width: cData.width,
        rotation: cData.rotation
    }
    */

    let card = await Card.implementation.fromDropData(data);
    console.log("IS THIS YOUR CARD")
    console.log(card)
    //const td = await actor.getTokenData({x: data.x, y: data.y, hidden: event.altKey});
}
