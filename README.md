##CARDS OPPOSING FOUNDRY
This game system for Foundry Virtual Tabletop is designed to provide a framework for playing the most awesome fill-in-the-blank party game designed for absolutely unforgivable horrid human beings.

If you like this, you should probably go buy several packets and boxes of cards from the creators of the much more original [Cards Against Humanity](https://www.cardsagainsthumanity.com/).

##PERMISSIONS
In accordance with the wishes of our card creating overlords and their legal advisors, this game system was created under the conditions that we do not in any way profit from it, that we give credit to them, that the system is released under the Creative Commons license (CC-BY-NC-SA 4.0), and that we do not in any way infringe upon their trademarks.

We are not associated with Cards Against Humanity LLC in any way. This game system is not endorsed by them, is in no way official, and there is explicitly no means by which to pay the creators of this Foundry VTT game system for using it or to gain any special access to more cards. The data for the cards enclosed in this game system wholly use the freely distributed cards provided at  [cardsagainsthumanity.com](https://www.cardsagainsthumanity.com/). No additional card data has been added from the numerous available Cards Against Humanity expansion packs (nor will such data be added.)

If you like the game, please support Cards Against Humanity LLC and BUY THEIR WORK. If you think it's fun in Foundry Virtual Tabletop, you should try it in Reality Physical Tabletop. You can buy the actual cards from the [Cards Against Humanity Shop](https://www.cardsagainsthumanity.com/shop/all).

