/**
 * The first attempt at a custom cards layer framework
 * to be eventually exported into a framework module
 */


class CanvasCards extends PIXI.Container {
    constructor(data) {
        //super(); probably unnecessary
        this.data = data;
        data.AThing = "It Worked!"
    }

    async draw() {
        // Define how this object should be rendered
    }
}


export class CardsLayer extends CanvasLayer {
    async draw() {
        await super.draw();
        this.objects = this.addChild(new PIXI.Container());
        const dataArray = canvas.scene.getFlag("CardsLayer", "CanvasCards");
        for ( let data of dataArray ) {
            const object = new CustomCanvasObject(data);
            await object.draw();
            this.objects.addChild(object);
        }
        return this;
    }
}

